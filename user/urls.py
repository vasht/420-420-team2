# -*- coding: utf-8 -*-
"""
This module contains the urls of the user app.
Created on Thu Apr 22 10:58:47 2021

@author: Vashti
"""
from django.urls import path
from . import views
from django.contrib.auth import views as auth_views

app_name = 'user'

urlpatterns = [
    path("register/", views.register, name='register'),
    path("login/", auth_views.LoginView.as_view(template_name="user/login.html"), name='login'),
    path("profile/", views.ProfileView.as_view(), name='profile'),
    path("logout/", auth_views.LogoutView.as_view(), name='logout'),
    path("profile/edit/", views.EditProfileView.as_view(), name='edit'),
    path("profile/change/", views.change, name='change')
    ]