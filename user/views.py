# Create your views here.
from django.shortcuts import render, redirect
from .forms import RegisterForm, EditUserProfileForm
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms  import PasswordChangeForm
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import generic

'''
This module defines the views for user app.
'''
'''
This is the register view. It will generate a 
registration form and creates a new user with the form.
'''
def register(request):
    if request.user.is_authenticated:
        return redirect("user:profile")
    
    context = {}
    form = RegisterForm()
    if request.method == "POST":
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "Account {} has been created. You may now login!"
                             .format(form.cleaned_data.get("username")))
            return redirect("user:login")
        else:
            context = {'form': form} #Form with request.Post
    else:
        context = {'form': form} #Form without request.Post
    return render(request, 'user/register.html', context)

'''
This is the profile view. The user must be logged in for it
to be accessible. It will display the user's information.
'''
class ProfileView(LoginRequiredMixin, generic.DetailView):
    login_url = "/login/"
    template_name =  "user/profile.html"
    
    #Set the object for this view as the current user
    def get_object(self):
        user = self.request.user
        return user
    
    def get_context_data(self, **kwargs):
        context = super(ProfileView, self).get_context_data(**kwargs)
        if self.request.user.is_superuser:
            context["base_template_name"] = "base_superuser.html"
        else:
            context["base_template_name"] = "base_user.html"     
        return context

'''
This is the edit profile view. It will allow the user to
modify their account's information, except username and password.
The user must be logged in to access this view.
'''
class EditProfileView(LoginRequiredMixin, generic.UpdateView):
    form_class = EditUserProfileForm
    template_name = "user/edit.html"
    success_url = "/profile/"
    
    #Set the object for this view as the current user
    def get_object(self):
        return self.request.user
    
    def get_context_data(self, **kwargs):
        context = super(EditProfileView, self).get_context_data(**kwargs)
        if self.request.user.is_superuser:
            context["base_template_name"] = "base_superuser.html"
        else:
            context["base_template_name"] = "base_user.html"     
        return context

'''
This is the change password view. It will allow the user to change their
password.
'''
@login_required(login_url="user:login")
def change(request):
    context = {}
    form = PasswordChangeForm(user=request.user)
    if request.method == "POST":
        form = PasswordChangeForm(data=request.POST, user=request.user)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)
            messages.success(request, "You have successfully changed your password.")
            return redirect("user:profile")
        else:
            messages.error(request, "Please correct the error.")
            
    context["form"] = form        
    if request.user.is_superuser:
        context["base_template_name"] = "base_superuser.html"
    else:
        context["base_template_name"] = "base_user.html"     
    
            
    return render(request, "user/change.html", context)


