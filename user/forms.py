# -*- coding: utf-8 -*-
"""
Created on Thu Apr 22 11:54:17 2021

@author: Vashti
"""
from .models import User
from django.contrib.auth.forms import UserCreationForm
from django import forms
import datetime

'''
This class is used to manage and create user register form.
'''
class RegisterForm(UserCreationForm):
    username = forms.CharField(min_length = 4, max_length = 25, help_text ="Must be unique, at least 4 characters, and " +
                               "no more than 25 characters.")
    email = forms.EmailField(max_length=60, label = "Email", widget = forms.EmailInput)
    dob = forms.DateField(label = "Date of birth", widget=forms.DateInput(format=('%Y-%m-%d'), 
                                                        attrs={'class': 'form-control', 'placeholder': 'Select a date',
                                                        'type': 'date'}))
    class Meta:
        model = User
        fields = ("username", "email", "first_name", "last_name", "dob", "password1", "password2")

    '''
    This function creates the form field values. It will create a new user
    and put it in the database.
    ---
    Returns:
        user
    '''
    def save(self, commit = True):
        user = super().save(commit=False)
        if commit:
            user.save()
        return user

    '''
    This function is used to validate the date of birth value
    from the form. It will throw a ValidationError if it is invalid.
    ---
    Returns
        dob
    '''
    def clean_dob(self):
        dob = self.cleaned_data.get("dob")
        if dob >= datetime.date.today():
            raise forms.ValidationError("The date cannot be the future.")
        if (datetime.date.today() - dob).days/365 < 18:
            raise forms.ValidationError("You must be over 18 to be a customer.")
        return dob
 
'''
This class is used to create a form to edit existing User information.
'''           
class EditUserProfileForm(forms.ModelForm):
    email = forms.EmailField(max_length=60, label = "Email", widget = forms.EmailInput)
    dob = forms.DateField(label = "Date of birth", widget=forms.DateInput(format=('%Y-%m-%d'), 
                                                        attrs={'class': 'form-control', 'placeholder': 'Select a date',
                                                        'type': 'date'}))
    bio = forms.CharField(max_length=250, widget=forms.Textarea)
    avatar = forms.ImageField(widget=forms.FileInput)
    
    '''
    The constructor will configure certain fields before generating form fields.
    '''
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['avatar'].required = False
        self.fields['bio'].required = False
        
    class Meta:
        model = User
        fields = ("email", "first_name", "last_name", "dob", "avatar", "bio")
      
    '''
    This function is used to validate the date of birth value
    from the form. It will throw a ValidationError if it is invalid.
    ---
    Returns
        dob
    '''
    def clean_dob(self):
        dob = self.cleaned_data.get("dob")
        if dob >= datetime.date.today():
            raise forms.ValidationError("The date cannot be the future.")
        if (datetime.date.today() - dob).days/365 < 18:
            raise forms.ValidationError("You must be over 18 to be a customer.")
        return dob
            
    '''
    This function is will change existing user field values with values
    from the form. It will save this change in the database.
    ---
    Returns
        user
    '''
    def save(self, commit=True):
        user = super().save(commit=False)
        user.email = self.cleaned_data.get("email")
        user.first_name = self.cleaned_data.get("first_name")
        user.last_name = self.cleaned_data.get("last_name")
        user.dob = self.cleaned_data.get("dob")
        user.avatar = self.cleaned_data.get("avatar")
        user.bio = self.cleaned_data.get("bio")
        
        if commit:
            user.save()
        return user
