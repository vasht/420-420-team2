'''
This module contains test cases for item app.
@author Vashti
'''

from django.test import TestCase
from .models import User
from django.urls import reverse
from .views import ProfileView, EditProfileView
import datetime

# Create your tests here.
user = User(username="test123_2", first_name="Johndyas", last_name="Cantrip")

class UserModelTest(TestCase):
    #test user's full name
    def test_get_full_name(self):
        expected = 'Johndyas Cantrip'
        self.assertEqual(user.get_full_name(), expected)
    
    #test user's short nam
    def test_get_short_name(self):
        expected = "Johndyas"
        self.assertEquals(user.get_short_name(), expected)
     
    #test user's to string method(username)    
    def test_to_string(self):
        expected = "test123_2"
        self.assertEquals(user.__str__(), expected)
