'''
This module is the model for user app.

@author Vashti
'''
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager

#Helper methods
def upload_to(instance, filename):
    # instance.user -> User model
    # intance.user.user -> User
    return "user/{}/{}".format(instance.username, filename)

#The models
'''
This class is used to handle account manangement. 
It extends to django's BaseUserModel
'''
class UserAccountManager(BaseUserManager):
    '''
    This method is used to create a new User.
    ---
    Parameters:
        username = username
        email = email
        first_name = first name
        last_name = last name
        password = password
        **other fields:
            dob = date of birth
    ---
    Returns:
        User - new User
    '''
    def create_user(self, username, email, first_name, last_name, password, **other_fields):
        if not email:
            raise ValueError("You must provide an email address.")
        
        user = self.model(username = username, email = self.normalize_email(email),
                          first_name = first_name, last_name = last_name, **other_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    '''
    This method is used to create a new superuser.
    ---
    Parameters:
        username = username
        email = email
        first_name = first name
        last_name = last name
        password = password
        **other fields:
            dob = date of birth
    ---
    Returns:
        User - new superuser
    '''
    def create_superuser(self, username, email, first_name, last_name, password, **other_fields):
        #Setting superuser permissions
        other_fields.setdefault('is_staff', True)
        other_fields.setdefault('is_superuser', True)
        
        return self.create_user(username, email, first_name, last_name, password, **other_fields)
'''
This class represents the User table. It creates a 
customer User(instead of django's User model) by extending
AbstractBaseUser and PermissionsMixin.
'''
class User(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(max_length = 25, unique = True)
    email = models.EmailField(max_length = 60, unique=True)
    first_name = models.CharField(max_length = 20)
    last_name = models.CharField(max_length = 20)
    dob = models.DateField()
    avatar = models.ImageField(blank = True, null = True, upload_to=upload_to)
    balance = models.PositiveIntegerField(default = 1000)
    bio = models.CharField(max_length = 250, blank = True, null = True)
    
    #Setting permissions
    is_staff = models.BooleanField(default = False)
    is_superuser = models.BooleanField(default = False)
    is_active = models.BooleanField(default = True)
    
    #Requirements for a KujujuUser
    USERNAME_FIELD = 'username'
    EMAIL_FIELD = 'email'
    REQUIRED_FIELDS = ['email', 'first_name', 'last_name', "dob"];
    
    #Obtain field values
    objects = UserAccountManager()
    
    '''
    This method returns the full name of the User. It overrides
    the django's User model get_full_name()
    ---
    Returns:
        User's first name and last name
    '''
    def get_full_name(self):
        return "{} {}".format(self.first_name, self.last_name)

    '''
    This method returns the full name of the User. It overrides
    the django's User model get_full_name()
    ---
    Returns:
        User's first name
    '''
    def get_short_name(self):
        return self.first_name
    
    '''
    This method returns the username of a User object.
    ----
    Returns:
        username - User's username
    '''
    def __str__(self):
        return self.username
    