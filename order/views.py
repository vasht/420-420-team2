from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseForbidden
from django.views import generic
from .models import Order
from item.models import Item

# Create your views here.

'''
This is the buy view. It will allow the user to buy an item. Only users
and non-owners can properly buy selected item.
'''
@login_required(login_url="user:login")    
def buy_item_view(request, pk):
    item = get_object_or_404(Item, item_id = pk, is_bought = False)
    context = {
        "user": request.user,
        "item": item
        }
    try:    
        if request.method == "POST":
            final_balance = request.user.balance - item.price
            if final_balance < 0:
                context["user_cant_buy"] = "Balance not enough to buy the item."
            else:     
                order = Order.objects.create(item=item,user=request.user)
                item.is_bought = True
                request.user.balance = final_balance
                item.owner.balance = item.owner.balance + item.price
                
                order.save()
                item.save()
                request.user.save()
                item.owner.save()
                return redirect("order:user_orders") #Return to user's order list page
    except (KeyError):
        return render(request, "order/create.html", context)
    
    if request.user.is_authenticated:
        if request.user.is_superuser:
            context["base_template_name"] = "base_superuser.html"
        else:
            context["base_template_name"] = "base_user.html"
    else:
        context["base_template_name"] = "base.html"
            
    return render(request, "order/create.html", context)
        
'''
This is the view for user's orders. It will only display items that they
have ordered.
'''    
class UserOrdersView(LoginRequiredMixin, generic.ListView):
    login_url = '/login/'
    template_name = 'order/orders.html'
    paginate_by = 5
    context_object_name = 'orders'
    
    def get_queryset(self):
        queryset = Order.objects.filter(user=self.request.user).order_by("-buy_date")
        return queryset
        
    def get_context_data(self, **kwargs):
        context = super(UserOrdersView, self).get_context_data(**kwargs)
        context["user"] = self.request.user
        if self.request.user.is_authenticated:
            if self.request.user.is_superuser:
                context["base_template_name"] = "base_superuser.html"
            else:
                context["base_template_name"] = "base_user.html"
        else:
            context["base_template_name"] = "base.html"
            
        return context
