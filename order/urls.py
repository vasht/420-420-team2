# -*- coding: utf-8 -*-

from django.urls import path, include
from . import views

app_name = 'order'

urlpatterns=[
    path("item/<int:pk>/buy/", views.buy_item_view, name='buy'),
    path("profile/orders", views.UserOrdersView.as_view(), name="user_orders")
    ]
