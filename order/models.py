from django.db import models
from item.models import Item
from user.models import User

# Create your models here.
'''
This class represents the Order table
'''
class Order(models.Model):
    order_id = models.AutoField(primary_key = True)
    user = models.ForeignKey(User, on_delete = models.CASCADE)
    item = models.ForeignKey(Item, on_delete = models.CASCADE)
    buy_date = models.DateTimeField(auto_now=True)
    
    def __str__(self):
        return "{}:{}".format(self.item.item_id, self.user.username)