from django.shortcuts import render, redirect, reverse
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import generic
from item.models import Category, Item
from django.db.models import Count
from django.http import Http404

# Create your views here.
'''
This is the index view. It is the main page of the website and
it will allow any users to view all items.
''' 
class IndexView(generic.ListView):
    template_name = 'search/index.html'
    paginate_by = 10
    context_object_name = 'items'
    
    def get_queryset(self):
        queryset = Item.objects.filter(is_bought=False).order_by("-creation_date")
        return queryset
    
    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context["categories"] = Category.objects.all()
        context["user"] = self.request.user
        
        if self.request.user.is_authenticated:
            if self.request.user.is_superuser:
                context["base_template_name"] = "base_superuser.html"
            else:
                context["base_template_name"] = "base_user.html"
        else:
            context["base_template_name"] = "base.html"
            
        return context

   
'''
This is the search view. It will show items based on a GET request
values.
''' 
class SearchView(generic.ListView):
    template_name = 'search/search.html'
    paginate_by = 10
    context_object_name = 'items'
    
    def get_queryset(self):
        try:
            search = self.request.GET
            queryset = Item.objects.filter(item_name__icontains = search.get("search_text"), is_bought=False)
            
            #Get specific items from category if category is specified
            if search.get("category") is not None:
                category = Category.objects.get(category_name = search.get("category"))
                queryset = queryset.filter(category = category)
            
            #Sort items if a sort option is elected
            sort = search.get("sort") 
            if sort is not None:
                if sort == "popular":
                    queryset = queryset.annotate(num_likes=Count('like')).order_by('-num_likes')
                elif sort == "newest":
                    queryset = queryset.order_by("-creation_date")
                elif sort == "oldest":
                    queryset = queryset.order_by("creation_date")
            
            return queryset
        except ValueError:
            raise Http404
            
    def get_context_data(self, **kwargs):
        context = super(SearchView, self).get_context_data(**kwargs)
        context["categories"] = Category.objects.all()
        context["user"] = self.request.user
        
        if self.request.user.is_authenticated:
            if self.request.user.is_superuser:
                context["base_template_name"] = "base_superuser.html"
            else:
                context["base_template_name"] = "base_user.html"
        else:
            context["base_template_name"] = "base.html"
        
        return context