# -*- coding: utf-8 -*-
"""
Created on Fri Apr 23 21:10:14 2021

@author: Vashti
"""

from . import views
from django.urls import path

app_name = 'item'

urlpatterns = [
    path("item/<int:pk>/", views.ItemView.as_view(), name="view"),
    path("item/create/", views.ItemCreateView.as_view(), name="create"),
    path("item/<int:pk>/edit/", views.ItemEditView.as_view(), name='edit'),
    path("item/<int:pk>/delete/", views.ItemDeleteView.as_view(), name='delete'),
    path("profile/listings", views.UserItemsView.as_view(), name='user_listings'),
    path("profile/sold", views.UserSoldView.as_view(), name='user_sold'),
    ]