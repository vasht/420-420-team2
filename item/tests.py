'''
This module contains test cases for item app.
@author Vashti
'''

from django.test import TestCase
from .models import Item
from user.models import User
from .views import ItemCreateView

customer_user1 = User(username = "test", is_active=True)
customer_user2 = User(username = "test2", is_active=True)
superuser1 = User(username = "test3", is_active=True, is_superuser=True)
superuser2 = User(username = "test4", is_active=True, is_superuser=True)


# Create your tests here.

'''
This class tests the Item model.
'''
class ItemModelTest(TestCase):
    #Test if item owner can edit
    def test_item_if_owner_can_edit(self):
        item = Item(owner = customer_user1)
        self.assertEquals(item.is_allowed_to_edit(customer_user1), True);
    
    #Test if non item owner can edit
    def test_item_if_not_owner_can_edit(self):
        item = Item(owner = customer_user1)
        self.assertEquals(item.is_allowed_to_edit(customer_user2), False);
    
    #Test if non item owner(but superuser) can edit
    def test_item_if_superuser_can_edit(self):
        item = Item(owner = customer_user1)
        self.assertEquals(item.is_allowed_to_edit(superuser1), True);
    
    #Test if item owner(but superuser) can edit
    def test_item_if_superuser_owner_can_edit(self):
        item = Item(owner = superuser1)
        self.assertEquals(item.is_allowed_to_edit(superuser1), True);
    
    #Test if a superuser can edit an item owned by a superuser  
    def test_item_if_superuser_not_superowner_can_edit(self):
        item = Item(owner = superuser1)
        self.assertEquals(item.is_allowed_to_edit(superuser2), True);
    
    #Test if a user can edit an item owned by a superuser  
    def test_item_if_user_not_superowner_can_edit(self):
        item = Item(owner = superuser1)
        self.assertEquals(item.is_allowed_to_edit(customer_user1), False);
        self.assertEquals(item.is_allowed_to_edit(customer_user2), False);
