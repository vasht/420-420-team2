from django.shortcuts import get_object_or_404
from .forms import ItemCreationForm, ItemEditForm
from .models import Item
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import generic
from django.urls import reverse

# Create your views here.
'''
This is the create item view. It allows the user to post an
item for sale. User must also be logged in to access this
page.
'''
class ItemCreateView(LoginRequiredMixin, generic.CreateView):
    login_url = "/login/"
    model = Item
    form_class = ItemCreationForm
    template_name = "items/create.html"
    
    #Set url when item creation is successful.
    def get_success_url(self):
        return reverse("item:view", kwargs ={"pk": self.pk})
    
    '''
    This method is called when user submits form and is valid. 
    It will save the item and associate the user as the owner
    '''
    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        self.pk = item.pk
        return super(ItemCreateView, self).form_valid(form)
    
    def get_context_data(self, **kwargs):
        context = super(ItemCreateView, self).get_context_data(**kwargs)
        context["user"] = self.request.user
        if self.request.user.is_superuser:
            context["base_template_name"] = "base_superuser.html"
        else:
            context["base_template_name"] = "base_user.html"    
        
        return context
    

'''
This is the create item view. It allows anyone to view an
item for sale. Non-logged in/specific users will not be able to get
full functionality like commenting, liking, editing, deleting the item.
'''
class ItemView(generic.DetailView):
    model = Item
    template_name = "items/view.html"
    
    #Set the context variable
    def get_context_data(self, **kwargs):
        context = super(ItemView, self).get_context_data(**kwargs)
        item = get_object_or_404(Item, item_id = self.kwargs["pk"], is_bought = False)
        context["item"] = item
        if self.request.user.is_authenticated:
            context['user_liked'] = len(item.like_set
                                    .filter(item=item, 
                                            user=self.request.user)) != 0
            
        context["comments"] = item.comment_set.all().order_by("-comment_date")
        if self.request.user.is_authenticated:
            if self.request.user.is_superuser:
                context["base_template_name"] = "base_superuser.html"
            else:
                context["base_template_name"] = "base_user.html"
        else:
            context["base_template_name"] = "base.html"
        return context

'''
This is the edit item view. It will allow the item owner to edit
the item's information. Only specific users will be able the item.
'''
class ItemEditView(generic.UpdateView):
    model = Item
    template_name = "items/edit.html"
    form_class = ItemEditForm
    
    #Set the context variable
    def get_context_data(self, **kwargs):
        context = super(ItemEditView, self).get_context_data(**kwargs)
        item = get_object_or_404(Item, item_id = self.kwargs["pk"], is_bought = False)
        
        #Verify if the user is allowed to edit
        if item.is_allowed_to_edit(self.request.user) == False: 
            context["user_not_allowed"] = "You are not allowed to view this page."
        
        if self.request.user.is_authenticated:     
            if self.request.user.is_superuser:
                context["base_template_name"] = "base_superuser.html"
            else:
                context["base_template_name"] = "base_user.html"
        else:
            context["base_template_name"] = "base.html"
            
        return context
    
    #Set successful url to be the item's view page
    def get_success_url(self):
        return reverse("item:view", kwargs={"pk": self.pk})
    
    
    def form_valid(self, form):
        item = form.save()
        self.pk = item.item_id
        return super(ItemEditView, self).form_valid(form)  


'''
This is the edit item view. It will allow the item owner to
delete their item. Only specific users will be able to view
this page.
'''
class ItemDeleteView(generic.DeleteView):
    model = Item
    template_name = "items/delete.html"
    success_url = "/"
    succuess_message = "Successfully deleted item."
    
    #Set the context variable
    def get_context_data(self, **kwargs):
        context = super(ItemDeleteView, self).get_context_data(**kwargs)
        item = get_object_or_404(Item, item_id = self.kwargs["pk"], is_bought = False)
        
        #Verify if the user is allowed to edit
        if item.is_allowed_to_edit(self.request.user) == False:
            context["user_not_allowed"] = "You are not allowed to view this page."
        context["item"] = item
        
        if self.request.user.is_authenticated:     
            if self.request.user.is_superuser:
                context["base_template_name"] = "base_superuser.html"
            else:
                context["base_template_name"] = "base_user.html"
        else:
            context["base_template_name"] = "base.html"
        return context
  
'''
This is the view for user's items. It will only display items that they
have posted that are not sold out.
'''    
class UserItemsView(LoginRequiredMixin, generic.ListView):
    login_url = '/login/'
    template_name = 'profile/items.html'
    paginate_by = 10
    context_object_name = 'items'
    
    def get_queryset(self):
        queryset = Item.objects.filter(owner=self.request.user, is_bought=False).order_by("-creation_date")
        return queryset
        
    def get_context_data(self, **kwargs):
        context = super(UserItemsView, self).get_context_data(**kwargs)
        context["user"] = self.request.user
        context["display_not_sold"] = True
        context["title"] = "Currently selling"
        if self.request.user.is_superuser:
            context["base_template_name"] = "base_superuser.html"
        else:
            context["base_template_name"] = "base_user.html"  
            
        return context

'''
This is the view for user's items. It will only display items that they
have posted that are sold out.
'''
class UserSoldView(LoginRequiredMixin, generic.ListView):   
    login_url = '/login/'
    template_name = 'profile/items.html'
    paginate_by = 10
    context_object_name = 'items'
    
    def get_queryset(self):
        queryset = Item.objects.filter(owner=self.request.user, is_bought=True).order_by("-creation_date")
        return queryset
        
    def get_context_data(self, **kwargs):
        context = super(UserSoldView, self).get_context_data(**kwargs)
        context["user"] = self.request.user
        context["display_not_sold"] = False
        context["title"] = "Sold"
        context["sold_view"] = True
        if self.request.user.is_superuser:
            context["base_template_name"] = "base_superuser.html"
        else:
            context["base_template_name"] = "base_user.html"    
        
        return context