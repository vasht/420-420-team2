'''
This is the model for item app.

@author Vashti
'''

from django.db import models
from user.models import User
import time

#Helper methods
'''
This method is used to create a unique name for the image.
---
Return:
    filename -> item/<date+time>.<file's extension>
'''   
def upload_to(instance, filename):
        ext = filename.split('.')[-1] #Go to the last index -> extension
        return 'items/%f.%s' % (time.time(), ext)
#Models
'''
This class represents the Category table.
'''
class Category(models.Model):
    category_id = models.AutoField(primary_key = True)
    category_name = models.CharField(max_length = 60)
    
    '''
    This method is used to return the name of the category
    when referring to the object.
    ---
    Returns:
        category_name = category id
    '''   
    def __str__(self):
        return self.category_name
    
    '''
    This sets the plural of "Category" to "Categories"
    '''
    class Meta:
        verbose_name_plural = "Categories"
    
'''
This class represents the Item table.
'''
class Item(models.Model):
    item_id = models.AutoField(primary_key = True)
    owner = models.ForeignKey(User, on_delete = models.CASCADE)
    price = models.DecimalField(decimal_places = 2, max_digits = 10)
    item_name = models.CharField(max_length = 25)
    description = models.TextField()
    category = models.ForeignKey(Category, on_delete= models.CASCADE)
    image = models.ImageField(upload_to=upload_to)
    is_bought = models.BooleanField(default = False)
    creation_date = models.DateTimeField(auto_now=True)
    
    '''
    This method is used to return the name of the item
    when referring to the object.
    ---
    Returns:
        item_name = Item name
    '''
    def __str__(self):
        return self.item_name
    
    def is_allowed_to_edit(self, user):
        if user.is_authenticated:
            return self.owner.username == user.username or user.is_superuser
        else:
            return False
     

