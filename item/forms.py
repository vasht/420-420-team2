# -*- coding: utf-8 -*-
"""
Created on Fri Apr 23 21:14:49 2021

@author: Vashti
"""

from .models import Item
from django import forms

#Forms
'''
This is the form for creating a new item
'''
class ItemCreationForm(forms.ModelForm):
    item_name = forms.CharField(min_length = 4)
    price = forms.DecimalField(decimal_places = 2, max_digits = 10)
    
    class Meta:
        model = Item
        fields = ("item_name", "price", "description", "category", "image")
    
    '''
    This checks if the price is not 0 or less. It will throw a
    ValidationError if invalid price is found
    ---
    Returns:
        price
    '''
    def clean_price(self):
        price = self.cleaned_data.get("price")
        if price <= 0:
            raise forms.ValidationError("Price cannot be 0 or less.")
        return price
    
    '''
    This method saves the item in the database.
    ---
    Returns:
        item
    '''
    def save(self, commit=True):
        item = super().save(commit=False)
        item.is_bought = False
        if commit:
            item.save()
        return item

'''
This class is a form for editing items.
'''
class ItemEditForm(forms.ModelForm):
    item_name = forms.CharField(min_length = 4)
    price = forms.DecimalField(decimal_places = 2, max_digits = 10)
    image = forms.ImageField(widget=forms.FileInput)
    
    class Meta:
        model = Item
        fields = ("item_name", "price", "description", "category", "image")
    
    '''
    This checks if the price is not 0 or less. It will throw a
    ValidationError if invalid price is found
    ---
    Returns:
        price
    '''
    def clean_price(self):
        price = self.cleaned_data.get("price")
        if price <= 0:
            raise forms.ValidationError("Price cannot be 0 or less.")
        return price

    '''
    This method saves the item in the database.
    ---
    Returns:
        item
    '''
    def save(self, commit=True):
        item = super().save(commit=False)
        item.item_name = self.cleaned_data.get("item_name")
        item.price = self.cleaned_data.get("price")
        item.description = self.cleaned_data.get("description")
        item.category = self.cleaned_data.get("category")
        item.image = self.cleaned_data.get("image")
        
        if commit:
            item.save()
        return item
    