from django.db import models
from item.models import Item
from user.models import User

# Create your models here.
'''
This class represents the Comment table.
'''
class Comment(models.Model):
    comment_id = models.AutoField(primary_key = True)
    item = models.ForeignKey(Item, on_delete = models.CASCADE)
    user = models.ForeignKey(User, on_delete = models.CASCADE)
    comment_text = models.TextField()
    comment_date = models.DateTimeField(auto_now=True)
    
    '''
    This method is used to return the item id and the username who commented
    when referring to the object
    ---
    Returns:
        comment_id - Comment id
    '''
    def __str__(self):
        return "{}:{}".format(self.item.item_id, self.user.username)