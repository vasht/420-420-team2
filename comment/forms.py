from django import forms
from .models import Comment
from item.models import Item
from user.models import User

'''
This is the form for creating a new comment.
'''
class CommentCreationForm(forms.ModelForm):
    comment_text = forms.CharField(min_length=1, widget=forms.Textarea)
    
    class Meta:
        model = Comment
        fields = ("comment_text",)
    
    def save(self, commit=True):
        comment = super().save(commit=False)
        if commit:
            comment.save()
        return comment
