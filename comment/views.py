from django.shortcuts import reverse, get_object_or_404
from .models import Comment
from item.models import Item
from django.views import generic
from .forms import CommentCreationForm
from django.contrib.auth.mixins import LoginRequiredMixin

# Create your views here.

'''
This is the create comment view. It will allow the user to add a comment
on an item, which will be displayed in the item view page.
'''
class CreateCommentView(LoginRequiredMixin, generic.CreateView):
    login_url = "/login/"
    model = Comment
    form_class = CommentCreationForm
    template_name = "comment/create.html"
    
    def get_context_data(self, **kwargs):
        context = super(CreateCommentView, self).get_context_data(**kwargs)
        item = get_object_or_404(Item, item_id = self.kwargs["pk"], is_bought = False)
        context["item"] = item
        context["user"] = self.request.user
        if self.request.user.is_superuser:
            context["base_template_name"] = "base_superuser.html"
        else:
            context["base_template_name"] = "base_user.html"  
            
        return context
    
    #Success url will be the item's view page
    def get_success_url(self):
        return reverse("item:view", kwargs={"pk": self.kwargs["pk"]})
    
    def form_valid(self, form):
        comment = form.save(commit=False)
        comment.user = self.request.user
        comment.item = get_object_or_404(Item, item_id = self.kwargs["pk"], is_bought = False)
        comment.save()
        return super(CreateCommentView, self).form_valid(form)  
    