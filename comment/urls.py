from django.urls import path
from . import views
from django.contrib.auth import views as auth_views

app_name = 'comment'


urlpatterns = [
    path("item/<int:pk>/comment", views.CreateCommentView.as_view(), name='comment'),
    ]