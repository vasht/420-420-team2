from django.shortcuts import render
from django.shortcuts import render, redirect, get_object_or_404
from .models import Like
from  item.models import Item
from django.contrib.auth.decorators import login_required

# Create your views here.
'''
This will allow the user to like or unlike an item. It will redirect the user
back to the liked/unliked item page.
'''
@login_required(login_url="user:login")
def item_liked(request, pk):
    item = get_object_or_404(Item, item_id = pk, is_bought = False)
    try: 
        #If user already have a like in this item, remove it
        like = Like.objects.get(item = item, user = request.user)
        like.delete()
    except Like.DoesNotExist: 
        
        #If no like by user to this item found, create one
        new_like = Like(item = item, user = request.user)
        new_like.save()  
    return redirect("item:view", pk=pk)     