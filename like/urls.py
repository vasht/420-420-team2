from django.urls import path
from . import views
from django.contrib.auth import views as auth_views

app_name = 'like'


urlpatterns = [
    path("item/<int:pk>/like", views.item_liked, name='like'),
    ]