from django.db import models
from item.models import Item
from user.models import User

# Create your models here.

'''
This is the Like Model. It keeps track of what users liked what item.
'''
class Like(models.Model):
    like_id = models.AutoField(primary_key = True)
    user = models.ForeignKey(User, on_delete = models.CASCADE)
    item = models.ForeignKey(Item, on_delete = models.CASCADE)
    
    def __str__(self):
        return "{}:{}".format(self.item.item_id, self.user.username)