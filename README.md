# 420-420-team2
Member(s): Vashti Lanz Rubio
Heroku website: http://kujuju.herokuapp.com/



Project info
---
This django project creates a website called Kujuju. It is a Kijiji-like website where users are able to buy/sell/view items posted by other customers, as well as their own items. Users will also be able to comment and like an item. Items are modifiable by the seller and  admins/superusers. It uses postgres database to store data, so it is required if this project will be run locally.

Setup (local):
---
Django part
1. Clone the repo.
2. in a CLI (ie anaconda powershell prompt), go to the cloned repo's **directory**. Make sure you are in the same directory as the **manage.py**.
3. type and enter **python manage.py migrate**


Postgres part (if you want to pre-populate the website) (make sure existing tables and table data won't interfere with the project)
1. Go to **db values** folder. There will be 6 files which will be used to import data into the postgres database.
2. With **pgAdmin**, go to PostgreSQL server -> Databases -> postgres -> Schemas -> Tables.
3. Import these files **in order**:
**(table name) = (file to import)**
- user_user = kijiji_like_user
- item_category = kijiji_like_category
- item_item = kijiji_like_item
- ****the rest can be in any order**
- like_like = kijiji_like_like
- order_order = kijiji_like_order
- comment_comment = kijiji_like_comment

**IMPORTANT**
If you did the Postgres part setup, run these commands in Postgres if you are planning to do interact with the webiste (create/comment/buy/etc.) because the table sequences will not be sync with the new data, which will cause IntegrityError when creating new items/orders/users/comments/likes.

```
SELECT setval('item_item_item_id_seq', (SELECT MAX(item_id) FROM item_item)+1);
SELECT setval('item_category_category_id_seq', (SELECT MAX(category_id) FROM item_category)+1);
SELECT setval('like_like_like_id_seq', (SELECT MAX(like_id) FROM like_like)+1);
SELECT setval('order_order_order_id_seq', (SELECT MAX(order_id) FROM order_order)+1);
SELECT setval('comment_comment_comment_id_seq', (SELECT MAX(comment_id) FROM comment_comment)+1);
SELECT setval('auth_group_id_seq', (SELECT MAX(id) FROM auth_group)+1);
SELECT setval('auth_group_permissions_id_seq', (SELECT MAX(id) FROM auth_group_permissions)+1);
SELECT setval('auth_permission_id_seq', (SELECT MAX(id) FROM auth_permission)+1);
SELECT setval('django_admin_log_id_seq', (SELECT MAX(id) FROM django_admin_log)+1);
SELECT setval('django_content_type_id_seq', (SELECT MAX(id) FROM django_content_type)+1);
SELECT setval('django_migrations_id_seq', (SELECT MAX(id) FROM django_migrations)+1);
SELECT setval('user_user_groups_id_seq', (SELECT MAX(id) FROM user_user_groups)+1);
SELECT setval('user_user_id_seq', (SELECT MAX(id) FROM user_user)+1);
SELECT setval('user_user_user_permissions_id_seq', (SELECT MAX(id) FROM user_user_user_permissions)+1);
COMMIT;
```


If there are no issues, run 
**python manage.py** runserver and the django website should be up and running at **http://localhost:8000**

Sample accounts
---
Admins

1. username: **vashti**; password: **password**
2. username: **admin**; password: **password**

Customers
1. username: **customer1**; password: **onepass123**
2. username: **customer2**; password: **twopass123**
3. username: **customer3**; password: **threepass123**
4. username: **customer4**; password: **fourpass123**
5. username: **customer5**; password: **fivepass123**

Visitors
1. non-logged in user

How to access admin page
--
2 ways:

**1. Access admin page in the website**
1. Run the django website locally or go to the kujuju heroku website.
2. Login with an admin account.
3. After logging in, the webite header should have a section called **Admin Page**. Click it.
4. You will be redirected to the admin page.

**2. Access admin page directly**
1. Run the djnago website locally or go to the kujuju heroku website.
2. Go to http://localhost:8000/admin in **local mode**. Go to http://kujuju.herokuapp.com/admin in **heroku**.
3. Login with an admin account or a created superuser account.
